CHANGELOG for CCK Taxonomy Subset for Drupal 6

cck_taxonomy_subset 6.x-1.3
 o Bug fix: taxonomy_super_select compatibility support

cck_taxonomy_subset 6.x-1.2
 o Documentation changes and change log addition
 o Bug fix: How this module works? http://drupal.org/node/438152

cck_taxonomy_subset 6.x-1.1
 o Bug fixes

cck_taxonomy_subset 6.x-1.0
 o Initial version


